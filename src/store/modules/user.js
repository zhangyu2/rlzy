import { getToken, setToken, removeToken } from '@/utils/auth.js'
// 导入login方法
import { loginAPI, getUserAPI, getUserDetailByIdAPI } from '@/api'

import { resetRouter } from '@/router'

export default {
  namespaced: true,
  // 公共数据
  state: {
    token: getToken() || null,
    user: {} // 用户信息

  },
  mutations: {
    // 获取token
    setToken(state, newToken) {
      state.token = newToken
      setToken(newToken) // 把token存储到本地
    },
    // 删除token
    remToken(state) {
      state.token = null
      removeToken() // 删除本地token
    },
    // 获取用户数据
    setuser(state, obj) {
      state.user = obj
    },
    // 删除用户数据
    removeuser(state) {
      state.user = {}
    }
  },
  actions: {
    // 退出登录
    logout(context) {
      // 清除token
      context.commit('remToken')
      // 清除用户信息
      context.commit('removeuser')
      // 重置路由信息
      resetRouter()
    },
    // 登录接口
    async  getUser(contxt, val) {
      const res = await loginAPI(val)
      contxt.commit('setToken', res.data)
    },
    // 获取用户数据请求
    async getUserProfile(contxt) {
      // 获取用户基本信息
      const res = await getUserAPI()
      const userId = res.data.userId // 获取用户id

      // 获取用户头像和权限信息
      const resDetail = await getUserDetailByIdAPI(userId)
      contxt.commit('setuser', { ...res.data, ...resDetail.data })

      return res.data.roles.menus
    }
  }
}
