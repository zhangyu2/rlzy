import { constantRoutes } from '@/router' // 静态路由
export default {
  namespaced: true,
  // 公共数据
  state: {
    // 本地取一下token
    menuList: [] // 所有可以访问的路由配置
  },
  mutations: {
    setMenuList(state, asyncRoutes) {
      state.menuList = [...constantRoutes, ...asyncRoutes]
    }
  }
}
