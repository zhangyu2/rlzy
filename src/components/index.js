import PageTools from '@/components/PageTools' // 注册全局组件
import UploadExcel from './excel/UploadExcel.vue'
import ScreenFull from '@/components/ScreenFull'
export default {
  install(Vue) {
    Vue.component('UploadExcel', UploadExcel)
    Vue.component('PageTools', PageTools)
    Vue.component('ScreenFull', ScreenFull)
  }
}
