import router, { asyncRoutes } from './router' // 路由
import store from './store' // vuex
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import getPageTitle from '@/utils/get-page-title'
const whiteList = ['/login', '/404'] // 白名单

router.beforeEach(async(to, form, next) => {
  // const tit = to.meta.title ? '人力资源管理平台-' + to.meta.title : '人力资源管理平台'
  // document.title = tit
  document.title = getPageTitle(to.meta.title)
  next()
})

// 全局前置路由守卫
router.beforeEach(async(to, from, next) => {
  NProgress.start()

  if (store.getters.token) {
    if (to.path === '/login') {
      next('/') // 禁止跳转
      console.log('已登陆，不能再访问登录界面')
    } else {
      // 判断是否有用户数据
      if (!store.getters.name) {
        // 获取到用户数据
        const menus = await store.dispatch('user/getUserProfile')

        const filterRoutes = asyncRoutes.filter(route => {
          return menus.includes(route.children[0].name)
        })

        // 把404放在最后边
        filterRoutes.push({ path: '*', redirect: '/404', hidden: true })

        // 把筛选好的路由 动态渲染
        router.addRoutes(filterRoutes)

        // 保存到vuex中 用来生成右侧栏
        store.commit('menu/setMenuList', filterRoutes)

        // 解决刷新出现的白屏bug
        next({
          ...to, // next({ ...to })的目的,是保证路由添加完了再进入页面 (可以理解为重进一次)
          replace: true // 重进一次, 不保留重复历史
        })
      } else {
        // 已登录,要去的不是登录界面，就直接放行
        next() // 可以跳转
      }
    }
  } else {
    // 没有token , 跳到指定页面
    if (whiteList.includes(to.path)) {
      // 要去登录界面放行
      next() // 可以跳转
    } else {
      // 去哪给页面都只能跳回到登录页面
      next('/login')
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
