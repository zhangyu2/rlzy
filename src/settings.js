module.exports = {

  title: '人力资源管理平台', // 页面的title，要重启才生效

  // /**
  //  * @type {boolean} true | false
  //  * @description Whether fix the header
  //  */
  fixedHeader: false,

  // /**
  //  * @type {boolean} true | false
  //  * @description Whether show the logo in sidebar
  //  */
  sidebarLogo: true // 显示logo
}
