// 导入axios实例
import request from '@/utils/request'

export function getRoles(params) { // 获取所有角色信息
  return request({
    url: '/sys/role',
    method: 'GET',
    'params': params
  })
}

export function addRole(data) { // 新增角色
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}

export function updateRole(data) { // 编辑角色
  return request({
    url: `/sys/role/${data.id}`,
    method: 'put',
    data
  })
}

export function deleteRole(id) { // 删除角色
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}

export function assignRoles(data) { //* *分配角色接口**
  return request({
    url: '/sys/user/assignRoles',
    data,
    method: 'put'
  })
}

export function getRoleDetail(id) { // 获取角色详细
  return request({
    url: `/sys/role/${id}`
  })
}

export function assignPerm(data) { // 给角色分配权限
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}
