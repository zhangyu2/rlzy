import request from '@/utils/request'

export function reqGetEmployeeList(params) { // 获取员工列表
  return request({
    methods: 'get',
    url: '/sys/user',
    params
  })
}

export function delEmployee(id) { // 删除员工
  return request({
    method: 'delete',
    url: `/sys/user/${id}`
  })
}

export function addEmployee(data) { // 新增员工
  return request({
    method: 'post',
    url: '/sys/user',
    data
  })
}

export function importEmployee(data) { // 实现excel导入
  return request({
    url: '/sys/user/batch',
    method: 'post',
    data
  })
}

