import request from '@/utils/request'

export function getDepartments() { // 获取组织人员信息
  return request({
    url: '/company/department'
  })
}

export function getEmployeeSimple() { // 获取员工列表
  return request({
    url: '/sys/user/simple'
  })
}

export function addDepartments(data) { // 新增部门
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

export function delDepartment(id) { // 删除部门
  return request({
    url: `/company/department/${id}`,
    method: 'delete'
  })
}

export function getDepartDetail(id) { // 获取部门信息
  return request({
    url: `/company/department/${id}`
  })
}

export function updateDepartments(data) { // 修改部门
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
