// 导入封装好的axios
import request from '@/utils/request'
export function getInfo(token) {
  console.log(request)
}

// 用户登录接口
// formData: {mobile:'', password:''}
export function loginAPI(formData) {
  return request({
    url: '/sys/login',
    method: 'POST',
    data: formData
  })
}

// 获取用户资料
export function getUserAPI() {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 获取用某个用户的信息
export function getUserDetailByIdAPI(id) {
  return request({
    url: `/sys/user/${id}`
  })
}

// 修改用户信息
export function saveUserDetailById(data) {
  return request({
    url: `/sys/user/${data.id}`,
    method: 'put',
    data
  })
}
