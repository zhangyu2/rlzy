// 导出一个axios的实例  而且这个实例要有请求拦截器 响应拦截器
import axios from 'axios'
import store from '@/store'
import router from '@/router'
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API
  // baseURL: 'http://ihrm-java.itheima.net' // 设置axios请求的基础的基础地址
}) // 创建一个axios的实例

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  // 对响应数据做点什么
  // console.log('响应数据', response)
  if (response.data.success) {
    // 本次操作，后端是成功的
    return response.data
  } else {
    // 抛出错误
    // 错误会在 try catch结构中的catch分支中捕获(err)
    return Promise.reject(response.data.message)
  }
}, function(error) {
  // 对响应错误做点什么
  if (error.response.data.code === 10002) {
    // token 过期,跳到登录页面,清除过期token
    store.commit('user/remToken')
    router.push('/login')
  }
  return Promise.reject(error)
})
// 添加响应拦截器

// 添加请求拦截器
service.interceptors.request.use(function(config) {
  // 先拿到vuex 里面的token

  if (store.state.user.token) {
    // 统一添加了Authorization头-携带身份标识
    config.headers.Authorization = 'Bearer ' + store.state.user.token
  }
  // 在发送请求之前做些什么
  return config
}, function(error) { // 对请求错误做些什么
  return Promise.reject(error)
})
// 添加请求拦截器
export default service // 导出axios实例
