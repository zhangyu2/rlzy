import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import '@/icons' // icon
import '@/permission' // permission control
import componets from '@/components'

Vue.directive(
  'allow', {
    inserted(el, binding) {
      const arr = store.state.user.user.roles.points
      if (arr.includes(binding.value)) {
        // el.style.display = 'none'
      } else {
        el.parentNode.removeChild(el)
      }
    }
  }

)

Vue.use(componets)
// set ElementUI lang to EN

Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app', // #app是public/index.html中的dom
  router,
  store,
  render: h => h(App)
})
